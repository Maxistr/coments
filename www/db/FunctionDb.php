<?php
namespace db;

use SQLite3;

interface iFunctionDb
{
    public function recordSaveDb(SQLite3 $db, array $SQLRequest);
    public function recordDeleteDb(SQLite3 $db, array $SQLRequest);
    public function recordSearchDb(SQLite3 $db, array $SQLRequest);
    public function recordUpdateDb(SQLite3 $db, array $SQLRequest);
}

class FunctionDb extends Db implements iFunctionDb
{
    public function recordDeleteDb(SQLite3 $db, array $SQLRequest)
    {
        $param = '';
        $count = 0;
        foreach ($SQLRequest as $key => $req) {
            if ($key !== 'table') {
                if ($count == 0) {
                    $param = $req;
                } else {
                    $param = $param . ' AND ' . $req;
                }
                $count++;
            }
        }

        if (!$db->query('DELETE FROM ' . $SQLRequest['table'] . ' WHERE ' . $param)) {
            return http_response_code(400);
        }
        else
        {
            return http_response_code(202);
        }
        // TODO: Implement recordDeleteDb() method.
    }

    public function recordSaveDb(SQLite3 $db, array $SQLRequest)
    {
        $param = '';
        $colum = '';
        $count = 0;
        foreach ($SQLRequest as $key => $req) {
            if ($key !== 'table') {
                if ($count == 0) {
                    $colum = $key;
                    $param = $req;
                } else {
                    $colum = $colum . ', '.$key;
                    if (is_string($req))
                    {
                        $param = $param . ', \'' . $req.'\'';
                    }
                    elseif (is_null($req)){
                        $param = $param . ', ' . 'NULL';
                    }
                    else {
                        $param = $param . ', ' . $req;
                    }
                }
                $count++;
            }
        }


        if(!$db->query('INSERT INTO ' . $SQLRequest['table'] . ' (' . $colum.') VALUES ('.
            $param.');'))
        {
            return http_response_code(400);
        }
        else
        {
            return http_response_code(201);
        }
    }

    public function recordSearchDb(SQLite3 $db, array $SQLRequest)
    {
        $field = '';
        $param = '';
        $count = 0;
        $count_field = 0;
        foreach ($SQLRequest as $key => $req) {
            if ($key !== 'table' && $key!=='limit' && $key!=='offset') {
                if (strpos($key,'field') === 0)
                {
                    if ($count_field == 0) {
                        $field = $req;
                    } else {
                        $field = $field . ', ' . $req;
                    }
                    $count_field++;
                }
                else {
                    if ($count == 0) {
                        $param = $req;
                    } else {
                        $param = $param . ' AND ' . $req;
                    }
                    $count++;
                }

            }
        }


        if (isset($SQLRequest['limit']) && isset($SQLRequest['offset']))
        {
            if ($param!='')
                {
                if (!$query = $db->query('SELECT ' . $field . ' FROM ' . $SQLRequest['table'] . ' WHERE ' . $param
                    . ' LIMIT ' . $SQLRequest['limit'] . ' OFFSET ' . $SQLRequest['offset'])) {
                    return http_response_code(400);
                } else {
                    $arr=array();
                    while($row=$query->fetchArray())
                    {
                        $arr[]=$row;
                    }
                    return $arr;
                }
            }
            else
            {
                if (!$query = $db->query('SELECT ' . $field . ' FROM ' . $SQLRequest['table']
                    . ' LIMIT ' . $SQLRequest['limit'] . ' OFFSET ' . $SQLRequest['offset'])) {
                    return http_response_code(400);
                } else {

                    $arr=array();
                    while($row=$query->fetchArray())
                    {
                        $arr[]=$row;
                    }
                    return $arr;
                }
            }
        }
        else {
            if ($param!='')
            {
                if (!$query =$db->query('SELECT ' . $field . ' FROM ' . $SQLRequest['table'] . ' WHERE ' . $param)) {
                    return http_response_code(400);
                } else {

                    $arr=array();
                    while($row=$query->fetchArray())
                    {
                        $arr[]=$row;
                    }
                    return $arr;
                }
            }
            else
            {
                if (!$query = $db->query('SELECT ' . $field . ' FROM ' . $SQLRequest['table'])) {
                    return http_response_code(400);
                } else {
                    $arr=array();
                    while($row=$query->fetchArray())
                    {
                        $arr[]=$row;
                    }
                    return $arr;
                }
            }
        }
    }

    public function recordUpdateDb(SQLite3 $db,array $SQLRequest)
    {
        $field = '';
        $param = '';
        $count = 0;
        $count_field = 0;
        foreach ($SQLRequest as $key => $req) {
            if ($key !== 'table' && $key!=='limit' && $key!=='offset') {
                if (strpos($key,'field') === 0)
                {
                    if ($count_field == 0) {
                        $field = $req;
                    } else {
                        $field = $field . ', ' . $req;
                    }
                    $count_field++;
                }
                else {
                    if ($count == 0) {
                        $param = $req;
                    } else {
                        $param = $param . ' AND ' . $req;
                    }
                    $count++;
                }
            }
        }

        if (!$db->query('UPDATE ' . $SQLRequest['table'] . ' SET ' . $field . ' WHERE ' . $param))
        {
            return http_response_code(400);
        }
        else
        {
            return http_response_code(202);
        }
    }
}