<?php

namespace db;

use SQLite3;

interface iDb
{
    public function connectDb(string $name);
    public function SQLQuery(SQLite3 $db, string $query);
}

Class Db implements iDb
{
    public function connectDb(string $name)
    {
        // TODO: Implement connectDb() method.
        if (is_string($name) && file_exists('db/' . $name . '.sqlite'))
        {
            $db = new SQLite3('db/' . $name . '.sqlite');
            return $db;
        }
        else
        {
            return http_response_code(500);
        }
    }

    public function SQLQuery(SQLite3 $db, string $query)
    {
     if (!$result=$db->query($query))
     {
         return http_response_code(500);
     }
     else
     {
         return $result->fetchArray();
     }
    }
}