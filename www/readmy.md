
Для запуска кода необходимо выполнить 
sudo docker-compose up в папке с проектом
**Содержимое index.php** 
$db = new FunctionDb();
$connectDb = $db->connectDb('identifier');
$com = new Comment($connectDb);
$com ->WorkComment();
При вызове WorkComment ожидает массив с параметрами 
параметры входных данных имеют следующий вид:
**Create** 
comment['message'=>'text_message']
Пример:
comment[message]=hi world112
**Delete**
comment['conditions'=>'text_condition]
Пример:
comment['conditions'=>'comment_id=1']
**Find**
comment['field'=>'name_field',
'conditions'=>'text_conditions'(необязательно),
'limit'=>'limit_number(необязательно если не указан offset),
'offset'=>'number_offset'(необязательно если не указан limit )]
**Update**
comment['update'=>'update_info',
        'condition'=>'text_condition']