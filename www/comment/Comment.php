<?php
namespace comment;

use comment\InputData;
use comment\OutputData;
use db\FunctionDb;
use SQLite3;

class Comment
{
    private $connect=null;
    private $comment_id=null;
    private $comment=null;

    public function __construct(SQLite3 $db)
    {
        $this->connect = $db;
    }

    private function GetComment()
    {
      $data = new InputData();

      if (is_array($data->POSTData()))
      {
          $this->comment = $data->POSTData();
      }
      elseif (is_array($data->GETData()))
      {
          $this->comment = $data->GETData();
      }
      else
      {
          http_response_code(500);
          return  http_response_code();
      }
    }

    public function AddComment()
    {
        //$this->GetComment();
        if (is_null($this->connect) || !isset($this->comment['message']) || is_null($this->comment))
        {
            return http_response_code(500);
        }
        else
        {
            $db = new FunctionDb();

            if (isset($comment['parent_id']))
            {
                $this->comment_id = $this->comment['id'];
            }
            $arr = ['table' => 'coment',
                    'comment_id'=>'NULL',
                    'parent_comment_id' => $this->comment_id,
                    'comment_message' => $this->comment['message']
                ];

            $db->recordSaveDb($this->connect,$arr);
        }
    }

    public function DeleteComment()
    {
        //$this->GetComment();
        if (is_null($this->connect) || !isset($this->comment['conditions']) || is_null($this->comment))
        {
            return http_response_code(500);
        }
        else
        {
            $db = new FunctionDb();
            $arr = ['table' => 'coment',
                    $this->comment['conditions']
                   ];

            $db->recordDeleteDb($this->connect,$arr);
        }
    }

    public function FindComment($Format_Out='text')
    {
        //$this->GetComment();
        if (is_null($this->connect) || !isset($this->comment['field'])|| is_null($this->comment))
        {
            return http_response_code(500);
        }
        else
        {
            $db = new FunctionDb();
            if (!isset($this->comment['limit']) && !isset($this->comment['offset']))
            {
                if (isset($this->comment['conditions'])) {
                    $arr = ['table' => 'coment',
                        'field' => $this->comment['field'],
                        $this->comment['conditions']
                    ];
                }
                else{
                    $arr = ['table' => 'coment',
                        'field' => $this->comment['field']
                    ];
                }

            }
            else
            {
                if (isset($this->comment['conditions']))
                {
                $arr = ['table' => 'coment',
                    'field' => $this->comment['field'],
                     $this->comment['conditions'],
                    'limit' => $this->comment['limit'],
                    'offset' => $this->comment['offset']
                ];
                }
                else
                {
                $arr = ['table' => 'coment',
                    'field' => $this->comment['field'],
                    'limit' => $this->comment['limit'],
                    'offset' => $this->comment['offset']
                ];
                }
            }
            //var_dump($arr);
            $out = new OutputData();
            if ($Format_Out=='text') {
                return $out->TextResponse($db->recordSearchDb($this->connect, $arr));
            }
            elseif ($Format_Out=='JASON')
            {
                return $out->JASONResponse($db->recordSearchDb($this->connect, $arr));
            }
            else
            {
                return http_response_code(500);
            }
        }
    }

    public function UpdateComment()
    {
      //  $this->GetComment();
        if (is_null($this->connect) || !isset($this->comment['update']) || !isset($this->comment['conditions']) || is_null($this->comment))
        {
            return http_response_code(500);
        }
        else
        {
            $db = new FunctionDb();
            $arr = ['table' => 'coment',
                $this->comment['conditions'],
                'field' => $this->comment['update']
            ];

            $db->recordUpdateDb($this->connect,$arr);
        }
    }

    public function WorkComment($Format_Out='text')
    {
        $this->GetComment();
        if (isset($this->comment['message']))
        {
            $this->AddComment();
            return http_response_code(200);
        }
        elseif (isset($this->comment['conditions']) && !isset($this->comment['field']) && !isset($this->comment['update']))
        {
            $this->DeleteComment();
            return http_response_code(200);
        }
        elseif (isset($this->comment['field']))
        {
            print_r($this->FindComment($Format_Out));
        }
        elseif (isset($this->comment['update']))
        {
            $this->UpdateComment();
            return http_response_code(200);
        }
        else
        {
            return http_response_code(500);
        }
    }
}