<?php
namespace comment;

class InputData
{
    public function GETData()
    {
        if (!isset($_GET['comment']))
        {
            http_response_code(500);
            return http_response_code();
        }
        else
        {
            return $_GET['comment'];
        }
    }

    public function POSTData()
    {
        if (!isset($_POST['comment']))
        {
            http_response_code(500);
            return http_response_code();
        }
        else
        {
            return $_POST['comment'];
        }
    }
}